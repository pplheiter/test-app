// api
export const API_SERVER_URL = "https://api.backendless.com";
export const API_ID =
  "53FB6874-BEE0-9546-FFCA-1F3DEE56BE00/73E38E38-C5C9-45B2-FFB5-D05A6E16A600";

// strings
export const STRING_APP_HEADER = "Test App";
export const STRING_WARNING_USER_DELETETION =
  "Are you sure you want to delete this user?";
export const STRING_TABLE_SEARCH = "Search";
export const STRING_TABLE_NEW_USER_BTN = "New User";
export const STRING_TABLE_MODAL_NEW_USER = "New User";
export const STRING_TABLE_MODAL_EDIT_USER = "Edit User";
export const STRING_TABLE_MODAL_SAVE_BTN = "Save";
export const STRING_TABLE_MODAL_CANCEL_BTN = "Cancel";
export const STRING_TABLE_MODAL_USER_NAME = "User Name";
export const STRING_TABLE_MODAL_EMAIL = "E-mail";
