import Vue from "vue";
import Vuex from "vuex";
import api from "@/config/api";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: null,
    showLoadingScreen: true,
    error: null
  },
  mutations: {
    set(state, { type, item }) {
      state[type] = item;
    }
  },
  actions: {
    enableLoadingScreen({ commit }) {
      commit("set", { type: "showLoadingScreen", item: true });
    },

    disableLoadingScreen({ commit }) {
      commit("set", { type: "showLoadingScreen", item: false });
    },

    setError({ commit }, text) {
      commit("set", { type: "error", item: text });
    },

    async getUsers({ commit, dispatch }) {
      dispatch("enableLoadingScreen");

      try {
        const res = await api.getUsers();

        commit("set", { type: "users", item: res.data });
        dispatch("disableLoadingScreen");
      } catch (error) {
        dispatch("disableLoadingScreen");
        dispatch("setError", "Error. Please try again later");
      }
    },

    async getUser({ dispatch }, objectID) {
      dispatch("enableLoadingScreen");

      try {
        await api.getUser(objectID);
        dispatch("disableLoadingScreen");
      } catch (error) {
        dispatch("disableLoadingScreen");
        dispatch("setError", "Error. Please try again later");
      }
    },

    async addUser({ dispatch }, data) {
      dispatch("enableLoadingScreen");

      try {
        await api.addUser({
          password: data.password,
          name: data.name,
          email: data.email
        });
        dispatch("disableLoadingScreen");
      } catch (error) {
        dispatch("disableLoadingScreen");
        dispatch("setError", "Server error or wrong e-mail format");
      }
    },

    async deleteUser({ dispatch }, objectId) {
      dispatch("enableLoadingScreen");

      try {
        await api.deleteUser({ objectId: objectId });
        dispatch("disableLoadingScreen");
      } catch (error) {
        dispatch("disableLoadingScreen");
        dispatch("setError", "Error. Please try again later");
      }
    },

    async updateUser({ dispatch }, data) {
      dispatch("enableLoadingScreen");

      try {
        await api.updateUser({
          password: data.password,
          name: data.name,
          email: data.email,
          objectId: data.objectId
        });
        dispatch("disableLoadingScreen");
      } catch (error) {
        dispatch("disableLoadingScreen");
        dispatch("setError", "Error. Please try again later");
      }
    }
  }
});
